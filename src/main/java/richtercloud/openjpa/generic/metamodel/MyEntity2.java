package richtercloud.openjpa.generic.metamodel;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author richter
 */
@MappedSuperclass
public abstract class MyEntity2<T extends MyEntity> implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    private T myEntity;

    public MyEntity2() {
    }

    public MyEntity2(MyEntity myEntity) {
        this.myEntity = this.myEntity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public T getMyEntity() {
        return myEntity;
    }

    public void setMyEntity(T myEntity) {
        this.myEntity = myEntity;
    }
}
