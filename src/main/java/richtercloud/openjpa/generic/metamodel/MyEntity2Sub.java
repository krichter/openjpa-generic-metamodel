/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package richtercloud.openjpa.generic.metamodel;

import javax.persistence.Entity;

/**
 *
 * @author richter
 */
@Entity
public class MyEntity2Sub extends MyEntity2<MyEntity> {
    private static final long serialVersionUID = 1L;

    public MyEntity2Sub() {
    }
    
    public MyEntity2Sub(MyEntity myEntity) {
        super(myEntity);
    }
}
